estados = {
  	"PA" : {
      "nome" : "Pará",
      "capital" : "Belém",
      "sigla" : "PA",
      "lat" : -3.678,
      "lon" : -52.361,
      "zoom" : 6,
      "regiao" : "Norte"
    },
    "RR" : {
      "nome" : "Roraima",
      "capital" : "Boa Vista",
      "sigla" : "RR",
      "lat" : 2.197,
      "lon" : -62.952,
      "zoom" : 7,
      "regiao" : "Norte"
    },
    "AP" : {
      "nome" : "Amapá",
      "capital" : "Macapá",
      "sigla" : "AP",
      "lat" : 1.274,
      "lon" : -52.718,
      "zoom" : 8,
      "regiao" : "Norte"
    },
    "AM" : {
      "nome" : "Amazonas",
      "capital" : "Manaus",
      "sigla" : "AM",
      "lat" : -4.620,
      "lon" : -64.611,
      "zoom" : 7,
      "regiao" : "Norte"
    },
    "AM" : {
      "nome" : "Amazonas",
      "capital" : "Manaus",
      "sigla" : "AM",
      "lat" : -4.620,
      "lon" : -64.611,
      "zoom" : 7,
      "regiao" : "Norte"
    },
    "TO" : {
      "nome" : "Tocantins",
      "capital" : "Palmas",
      "sigla" : "TO",
      "lat" : -10.282,
      "lon" : -49.559,
      "zoom" : 7,
      "regiao" : "Norte"
    },
    "RO" : {
      "nome" : "Rondônia",
      "capital" : "Porto Velho",
      "sigla" : "RO",
      "lat" : -10.876,
      "lon" : -64.017,
      "zoom" : 7,
      "regiao" : "Norte"
    },
    "AC" : {
      "nome" : "Acre",
      "capital" : "Rio Branco",
      "sigla" : "AC",
      "lat" : -9.590,
      "lon" : -71.917,
      "zoom" : 7,
      "regiao" : "Norte"
    },
    "SE" : {
      "nome" : "Sergipe",
      "capital" : "Aracaju",
      "sigla" : "SE",
      "lat" : -10.4311,
      "lon" : -38.1171,
      "zoom" : 9,
      "regiao" : "Nordeste"
    },
    "CE" : {
      "nome" : "Ceará",
      "capital" : "Fortaleza",
      "sigla" : "CE",
      "lat" : -4.976,
      "lon" : -40.671,
      "zoom" : 8,
      "regiao" : "Nordeste"
    },
    "PB" : {
      "nome" : "Paraíba",
      "capital" : "João Pessoa",
      "sigla" : "PB",
      "lat" : -6.9210,
      "lon" : -36.5762,
      "zoom" : 9,
      "regiao" : "Nordeste"
    },
    "AL" : {
      "nome" : "Alagoas",
      "capital" : "Maceió",
      "sigla" : "AL",
      "lat" : -9.4762,
      "lon" : -37.1146,
      "zoom" : 9,
      "regiao" : "Nordeste"
    },
    "RN" : {
      "nome" : "Rio Grande do Norte",
      "capital" : "Natal",
      "sigla" : "RN",
      "lat" : -5.3727,
      "lon" : -36.7081,
      "zoom" : 9,
      "regiao" : "Nordeste"
    },
    "PE" : {
      "nome" : "Pernambuco",
      "capital" : "Natal",
      "sigla" : "PE",
      "lat" : -8.271,
      "lon" : -38.447,
      "zoom" : 8,
      "regiao" : "Nordeste"
    },
    "BA" : {
      "nome" : "Bahia",
      "capital" : "Salvador",
      "sigla" : "BA",
      "lat" : -13.315,
      "lon" : -43.550,
      "zoom" : 7,
      "regiao" : "Nordeste"
    },
    "MA" : {
      "nome" : "Maranhão",
      "capital" : "São Luís",
      "sigla" : "MA",
      "lat" : -5.364,
      "lon" : -45.121,
      "zoom" : 7,
      "regiao" : "Nordeste"
    },
    "MS" : {
      "nome" : "Mato Grosso do Sul",
      "capital" : "Campo Grande",
      "sigla" : "MS",
      "lat" : -20.427,
      "lon" : -56.305,
      "zoom" : 7,
      "regiao" : "Centro-Oeste"
    },
    "MT" : {
      "nome" : "Mato Grosso",
      "capital" : "Cuiabá",
      "sigla" : "MT",
      "lat" : -13.891,
      "lon" : -56.733,
      "zoom" : 7,
      "regiao" : "Centro-Oeste"
    },
    "GO" : {
      "nome" : "Goias",
      "capital" : "Goiania",
      "sigla" : "GO",
      "lat" : -15.665,
      "lon" : -50.526,
      "zoom" : 7,
      "regiao" : "Centro-Oeste"
    },
    "MG" : {
      "nome" : "Minas Gerais",
      "capital" : "Belo Horizonte",
      "sigla" : "MG",
      "lat" : -19.073,
      "lon" : -46.417,
      "zoom" : 7,
      "regiao" : "Sudeste"
    },
    "RJ" : {
      "nome" : "Rio de Janeiro",
      "capital" : "Rio de Janeiro",
      "sigla" : "RJ",
      "lat" : -21.963,
      "lon" : -43.583,
      "zoom" : 8,
      "regiao" : "Sudeste"
    },
    "SP" : {
      "nome" : "São Paulo",
      "capital" : "São Paulo",
      "sigla" : "SP",
      "lat" : -21.933,
      "lon" : -49.856,
      "zoom" : 7,
      "regiao" : "Sudeste"
    },
    "ES" : {
      "nome" : "Espírito Santo",
      "capital" : "Vitória",
      "sigla" : "ES",
      "lat" : -19.674,
      "lon" : -41.490,
      "zoom" : 8,
      "regiao" : "Sudeste"
    },
    "PR" : {
      "nome" : "Paraná",
      "capital" : "Curitiba",
      "sigla" : "PR",
      "lat" : -24.407,
      "lon" : -53.152,
      "zoom" : 7,
      "regiao" : "Sul"
    },
    "SC" : {
      "nome" : "Santa Catarina",
      "capital" : "Florianópolis",
      "sigla" : "SC",
      "lat" : -27.362,
      "lon" : -51.965,
      "zoom" : 8,
      "regiao" : "Sul"
    },
    "RS" : {
      "nome" : "Rio Grande do Sul",
      "capital" : "Porto Alegre",
      "sigla" : "RS",
      "lat" : -29.831,
      "lon" : -54.492,
      "zoom" : 7,
      "regiao" : "Sul"
    },
    "DF" : {
      "nome" : "Distrito Federal",
      "capital" : "Brasília",
      "sigla" : "DF",
      "lat" : -15.8028,
      "lon" : -47.9443,
      "zoom" : 10,
      "regiao" : "Sul"
    }

}